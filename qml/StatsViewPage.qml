import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Shellexec 1.0
//import Indicator 1.0

Page {

    header: PageHeader {
        id: header
        title: "uStats"

        trailingActionBar.actions: [
            Action {
                iconName: "edit-clear"
                text: i18n.tr("Quit")
                onTriggered: {
                    Qt.quit()
                }
            },
            Action {
                iconName: "info"
                text: i18n.tr("About")
                onTriggered: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            },
            Action {
                iconName: "reload"
                text: i18n.tr("Reload data")
                onTriggered: {
                  memtotal.text = parseVmstatOutput(0)
                  memused.text = parseVmstatOutput(1)
                  memfree.text = parseVmstatOutput(4)
                  cachetotal.text = parseVmstatOutput(7)
                  cacheused.text = parseVmstatOutput(8)
                  cachefree.text = parseVmstatOutput(9)
                  texttodaytotal.text = parseVnstatOutput(5).toString()
                  textcurrentmonthtotal.text = parseVnstatOutput(10).toString()
                  textlastmonthtotal.text = parseVnstatOutput(14).toString()
                }
              }
        ]
    }

    Shellexec {
      id: launcher
    }

    function parseVmstatOutput(lineIndex) {
      //read output of command 'vmstat -stats'
      var output = launcher.launch("vmstat -stats").split("\n");
      var value = output[lineIndex].substr(0,13).replace(" ","");
      return value
    }

    function parseVnstatOutput(lineIndex) {
      //read output of command 'vnstat --online'
      var output = launcher.launch("vnstat --oneline").split(";");
      var value = output[lineIndex];
      var bothMonths
      var thisMonth
      var lastMonth
      var bothMonthsUnit
      var thisMonthUnit
      var lastMonthUnit
      if (lineIndex === 14) {
        //calculate the total of last month from the diff of both months minus current month
        bothMonths = output[14].split(" ")[0].replace(",",".");
        bothMonthsUnit= output[14].split(" ")[1].substr(0,3);
        thisMonth = output[10].split(" ")[0].replace(",",".");
        thisMonthUnit= output[10].split(" ")[1].substr(0,3);
        if (bothMonthsUnit == thisMonthUnit) {
          lastMonthUnit = thisMonthUnit;
        }
        if (bothMonthsUnit === "GiB" && thisMonthUnit === "MiB"){
          lastMonthUnit = bothMonthsUnit;
          thisMonth = thisMonth/1000;
        }
        if (bothMonthsUnit.toString() === "GiB" && thisMonthUnit.toString() === "KiB"){
          lastMonthUnit = bothMonthsUnit;
          thisMonth = thisMonth/1000000;
        }
        lastMonth = Math.round((bothMonths-thisMonth)*100)/100;
        value = lastMonth.toString().replace(".",Qt.locale(Qt.locale().name).decimalPoint) + " " + lastMonthUnit;
      }
      return value.replace("\n","")
      //[0] number of ?
      //[1] interface
      //[2] date of last update
      //[3] today download
      //[4] today upload
      //[5] today total
      //[6] today rate
      //[7] current month's name
      //[8] current month's download
      //[9] current month's upload
      //[10] current month's total
      //[11] current month's rate
      //[12] current month + last month download
      //[13] current month + last month upload
      //[14] current month + last month total
    }

    Flickable {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        contentHeight: vmstatdatagrid.height + buttonColumn.height + units.gu(10)

        GridLayout {
            id: vmstatdatagrid
            columns: 3

            anchors {
                    left: parent.left;
                    top: parent.top;
                    right: parent.right;
                    margins: units.gu(2)
                }

            Text {
              text: i18n.tr("Selected values of") + " \'vmstat -stats\' :";
              Layout.columnSpan: 3;
              font.bold: true;
              color: theme.palette.normal.baseText;
            }

            Text { text: i18n.tr("total memory"); color: theme.palette.normal.foregroundText;}
            Text {
              id: memtotal;
              text: parseVmstatOutput(0);
              color: theme.palette.normal.foregroundText;
            }
            Text { text: "K"; color: theme.palette.normal.foregroundText;}

            Text { text: i18n.tr("used memory");color: theme.palette.normal.foregroundText;}
            Text {
              id: memused;
              text: parseVmstatOutput(1);
              color: theme.palette.normal.foregroundText;
            }
            Text { text: "K"; color: theme.palette.normal.foregroundText;}

            Text { text: i18n.tr("free memory");color: theme.palette.normal.foregroundText;}
            Text {
              id: memfree;
              text: parseVmstatOutput(4);
              color: theme.palette.normal.foregroundText;
            }
            Text { text: "K"; color: theme.palette.normal.foregroundText;}

            Text { text: i18n.tr("total cache");color: theme.palette.normal.foregroundText;}
            Text {
              id: cachetotal;
              text: parseVmstatOutput(7);
              color: theme.palette.normal.foregroundText;
            }
            Text { text: "K"; color: theme.palette.normal.foregroundText;}

            Text { text: i18n.tr("used cache");color: theme.palette.normal.foregroundText;}
            Text {
              id: cacheused;
              text: parseVmstatOutput(8);
              color: theme.palette.normal.foregroundText;
            }
            Text { text: "K"; color: theme.palette.normal.foregroundText;}

            Text { text: i18n.tr("free cache");color: theme.palette.normal.foregroundText;}
            Text {
              id: cachefree;
              text: parseVmstatOutput(9);
              color: theme.palette.normal.foregroundText;
            }
            Text { text: "K"; color: theme.palette.normal.foregroundText;}
        }

        ColumnLayout {
            id: buttonColumn
            anchors {
              left: parent.left
              top: vmstatdatagrid.bottom
              right: parent.right
              margins: units.gu(2)
            }
            spacing: units.gu(1)

            Button {
              id: vmstatButton
              height: units.gu(5)
              width: units.gu(20)
              anchors {
                topMargin: units.gu(2)
                leftMargin: units.gu(1)
                horizontalCenter: parent.horizontalleft
              }
              color: theme.palette.normal.base
              text: i18n.tr("show more memory statistics")
              onTriggered: {
                pageStack.push(Qt.resolvedUrl("VmstatViewPage.qml"))
              }
            }

            //divider
            Rectangle {
            height: 2
            color: theme.palette.normal.foregroundText
            anchors {
                left: parent.left
                right: parent.right
                topMargin: units.gu(2)
                bottomMargin: units.gu(2)
                top: vmstatButton.bottom
                }
            }

            Label {
              id: errorLabel
              anchors {
                left: parent.left;
                top: vmstatButton.bottom;
                right: parent.right;
                topMargin: units.gu(4)
                bottomMargin: units.gu(2)
                }
              visible: parseVnstatOutput(1) === undefined ? true: false
              text: i18n.tr("vnstat needs to be installed to retrieve data\nfor instructions see readme of uStats repo on gitlab:\nhttps://gitlab.com/Danfro/ustats")
            }

            GridLayout {
                id: vnstatgrid
                columns: 2
                visible: parseVnstatOutput(1) === undefined ? false: true

                anchors {
                  left: parent.left;
                  top: vmstatButton.bottom;
                  right: parent.right;
                  topMargin: units.gu(4)
                  bottomMargin: units.gu(2)
                  }

                  Text {
                    text: i18n.tr("Selected values of") + " \'vnstat\' :";
                    Layout.columnSpan: 2;
                    font.bold: true;
                    color: theme.palette.normal.baseText;
                  }

                  Text { text: i18n.tr("interface: ");color: theme.palette.normal.foregroundText;}
                  Text { text: parseVnstatOutput(1);color: theme.palette.normal.foregroundText;}

                  Text { text: i18n.tr("data last updated") + ": ";color: theme.palette.normal.foregroundText;}
                  Text { text: parseVnstatOutput(2);color: theme.palette.normal.foregroundText;}

                  Text { text: i18n.tr("today total") + ": ";color: theme.palette.normal.foregroundText;}
                  Text {
                    id: texttodaytotal;
                    text: parseVnstatOutput(5).toString();
                    color: theme.palette.normal.foregroundText;
                  }

                  Text { text: i18n.tr("this month total") + ": ";color: theme.palette.normal.foregroundText;}
                  Text {
                    id: textcurrentmonthtotal;
                    text: parseVnstatOutput(10).toString();
                    color: theme.palette.normal.foregroundText;
                  }

                  Text { text: i18n.tr("last month total") + ": ";color: theme.palette.normal.foregroundText;}
                  Text {
                    id: textlastmonthtotal;
                    text: parseVnstatOutput(14).toString();
                    color: theme.palette.normal.foregroundText;
                  }
                }

            Button {
              id: vnstatButton
              height: units.gu(5)
              width: units.gu(20)
              anchors {
                topMargin: units.gu(2)
                leftMargin: units.gu(2)
                horizontalCenter: parent.horizontalleft
                top: vnstatgrid.bottom
              }
              color: theme.palette.normal.base
              text: i18n.tr("show detailed data usage statistics")
              onTriggered: {
                pageStack.push(Qt.resolvedUrl("VnstatViewPage.qml"));
              }
            }


          //divider
          Rectangle {
          height: 2
          color: theme.palette.normal.foregroundText
          anchors {
              left: parent.left
              right: parent.right
              topMargin: units.gu(2)
              bottomMargin: units.gu(2)
              top: vnstatButton.bottom
              }
          }


          Button {
            id: customButton
            height: units.gu(5)
            width: units.gu(20)
            anchors {
              top: vnstatButton.bottom
              topMargin: units.gu(4)
              leftMargin: units.gu(2)
              horizontalCenter: parent.horizontalleft
            }
            color: theme.palette.normal.base
            text: i18n.tr("get output of custom commands")
            onTriggered: {
              pageStack.push(Qt.resolvedUrl("CustomCommandPage.qml"));
            }
          }
        }
      }
}
